package com.abinash.utilities;

import com.abinash.interfaces.IAddition;

public class Addition implements IAddition {

	public int addIntegers(int a, int b) {
		return a + b;
	}

}
